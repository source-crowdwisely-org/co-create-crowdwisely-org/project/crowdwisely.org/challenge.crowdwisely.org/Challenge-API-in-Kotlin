package org.crowdwisely.challenge.challengeapiinkotlin

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ChallengeApiInKotlinApplication

fun main(args: Array<String>) {
	runApplication<ChallengeApiInKotlinApplication>(*args)
}
